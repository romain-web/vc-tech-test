import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'
import Products from '../views/Products.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/products/:brand?',
    name: 'Products',
    component: Products,
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
