import Vue from 'vue'
import VueUi from '@vue/ui'

import App from './App.vue'
import router from './router'

Vue.use(VueUi)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
